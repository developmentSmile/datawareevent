function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    //21.103319, -101.636239
    center: {lat: 21.103319, lng: -101.636239},
    zoom: 15
});

  var iconBase = 'http://marketing-queretaro.com/markers/';

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(21.103319, -101.636239),
    map: map,
    icon: iconBase + '32x32amarillo.png',
    title: '¡Encuentranos aquí!'
  });

}

