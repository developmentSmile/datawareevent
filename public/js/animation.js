$(document).ready(function() {
"use strict";
    $("#owl-demo").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 4,
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
 
  });

  // Back to Top
  $('.top').click(function(event) {
    $("html,body").animate({ scrollTop: 0 }, 2000);
    event.preventDefault();
  });
  
  /*// InView
  var $fadeInDown = $('.menu, .header h1, .header .subtitle, .topics h3, .topics div i, .speakers .single h3');
  var $fadeInLeft = $('.when, .where, .speakers h2, .speakers .featured h3, .schedule h2, .bullets h3, .registration h2, .registration .form, .sponsors h2, .location h2, .maps .images, .maps #map_canvas, .social h2');
  var $fadeInRight = $('.register-now, .speakers .subtitle, .schedule .subtitle, .registration .subtitle, .registration .price, .sponsors .subtitle, .location .subtitle, .location .address, .social .subtitle');

  $fadeInDown.css('opacity', 0);
  $fadeInLeft.css('opacity', 0);
  $fadeInRight.css('opacity', 0);

  // InView - fadeInDown
  $fadeInDown.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInDown'); }
  });

  // InView - fadeInLeft
  $fadeInLeft.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInLeft'); }
  });

  // InView - fadeInRight
  $fadeInRight.one('inview', function(event, visible) {
    if (visible) { $(this).addClass('animated fadeInRight'); }
  });*/



});