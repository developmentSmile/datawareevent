<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SponsorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sponsors')->insert([
            'name' => 'Zebra',
            'image' => 'logo_zebra-01.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('sponsors')->insert([
            'name' => 'InAction',
            'image' => 'Logo_In-Action-01.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('sponsors')->insert([
            'name' => 'SAP',
            'image' => 'logo_sap-04.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('sponsors')->insert([
            'name' => 'CICEG',
            'image' => 'CICEG_LOGO-01.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
