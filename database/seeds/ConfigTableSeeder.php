<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            'title'         => 'Conferencia: Convierte tu empresa en un negocio más rentable.',
            'description'   => 'Asiste a nuestra conferencia / desayuno gratuito en León. Hablaremos de cómo hacer tu empresa más rentable y eficientar tus procesos. Regístrate aquí!',
            'keywords'      => 'Industria del Calzado,Conferencia,Eventos león,Pymes,Soluciones tecnológicas,SAP,ZEBRA',
            'whenString'    => 'Desayuno',
            'whereMain'     => 'CICEG',
            'whereSecond'   => 'León, Gto.',
            'actionButton'  => 'Regístrate Ahora',
            'leads'         => 'marketing@dataware.com.mx,n.sandoval@dataware.com.mx,datawaremkt@gmail.com',
            'facebook'      => 'https://www.facebook.com/DataWareSoluciones/',
            'twitter'       => 'https://twitter.com/DataWare_?lang=es',
            'googleplus'    => 'https://plus.google.com/u/1/101446802068712887694?hl=es',
            'linkedin'      => 'https://www.linkedin.com/company-beta/10827633/',
            'youtube'       => 'https://www.youtube.com/channel/UCx6438BOMVEk9B7Gh5F2rSw',
            'phone'         => '+52 (442) 24 51 535',
            'email'         => 'marketing@dataware.com.mx',
            'url'           => 'www.dataware.com.mx',
            'when'          => 'Jueves 20 de Julio',
            'lat'           => '21.103319',
            'long'          => '-101.636239',
            'duration'          => 1,
        ]);
    }
}