<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ScheduleTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '08:30',
            'endHour' => '09:00',
            'title' => 'Recepción del evento',
            'speaker' => '',
            'position' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '09:00',
            'endHour' => '09:10',
            'title' => 'Apertura del evento',
            'speaker' => 'Lic. Luis Gerardo González.',
            'position' => 'Director del CICEG.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '09:10',
            'endHour' => '09:40',
            'title' => 'PROCESOS I',
            'speaker' => 'M.C. Enrique Kato Miranda.',
            'position' => 'Director de Soluciones Tecnológicas del CIATEC.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '09:40',
            'endHour' => '09:55',
            'title' => 'FINANZAS',
            'speaker' => 'Lic. Héctor López.',
            'position' => 'SAP OEM Manager.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '09:55',
            'endHour' => '10:10',
            'title' => 'PROCESOS II',
            'speaker' => 'M.C. Enrique Kato Miranda.',
            'position' => 'Director de Soluciones Tecnológicas del CIATEC.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '10:10',
            'endHour' => '10:25',
            'title' => 'INTEGRACIÓN',
            'speaker' => 'Lic.Fernando Tavera y Blanca Benítez.',
            'position' => 'Consultor SAP y CHAM Zebra.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('schedule')->insert([
            'day' => '20-07-2017',
            'startHour' => '10:25',
            'endHour' => '11:30',
            'title' => 'Meet-up - Exposición de Stands.',
            'speaker' => '',
            'position' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
