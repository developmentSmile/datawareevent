<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ana María González',
            'email' => 'ana.gonzalez@smilemarketing.mx',
            'password' => Hash::make('secret'),
            'remember_token' => str_random(10),
            'created_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'name' => 'Ana María González',
            'email' => 'n.sandoval@dataware.com.mx',
            'password' => Hash::make('eventsecret'),
            'remember_token' => str_random(10),
            'created_at' => Carbon::now()
        ]);
    }
}
