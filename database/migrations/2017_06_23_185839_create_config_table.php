<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('description');
            $table->string('keywords');
            $table->string('when');
            $table->string('whenString');
            $table->string('whereMain');
            $table->string('whereSecond');
            $table->string('actionButton');
            $table->string('lat');
            $table->string('long');
            $table->string('leads'); //correo 
            $table->string('facebook');
            $table->string('twitter');
            $table->string('googleplus');
            $table->string('linkedin');
            $table->string('youtube');
            $table->string('phone');
            $table->string('email');
            $table->string('url');
            $table->integer('duration');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config');
    }
}
