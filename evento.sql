-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 17-07-2017 a las 23:08:19
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `datac005_evento`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `when` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whenString` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whereMain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whereSecond` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actionButton` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leads` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googleplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `title`, `description`, `keywords`, `when`, `whenString`, `whereMain`, `whereSecond`, `actionButton`, `lat`, `long`, `leads`, `facebook`, `twitter`, `googleplus`, `linkedin`, `youtube`, `phone`, `email`, `url`, `duration`, `created_at`, `updated_at`) VALUES
(1, 'Conferencia: Tendencias de TI en Manufactura', 'Asiste a nuestra conferencia / desayuno gratuito en León. Hablaremos de cómo hacer tu empresa más rentable y eficientar tus procesos. Regístrate aquí!', 'Industria del Calzado,Conferencia,Eventos león,Pymes,Soluciones tecnológicas,SAP,ZEBRA', 'Jueves 10 de Agosto', '8:30 a 13:30', 'G100 Business District', 'Silao, Gto.', 'Regístrate Ahora', '21.103319', '-101.636239', 'marketing@dataware.com.mx,n.sandoval@dataware.com.mx,datawaremkt@gmail.com', 'https://www.facebook.com/DataWareSoluciones/', 'https://twitter.com/DataWare_?lang=es', 'https://plus.google.com/u/1/101446802068712887694?hl=es', 'https://www.linkedin.com/company-beta/10827633/', 'https://www.youtube.com/channel/UCx6438BOMVEk9B7Gh5F2rSw', '+52 (447) 367 6991', 'marketing@dataware.com.mx', 'www.dataware.com.mx', 1, NULL, '2017-07-07 23:13:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_06_23_185839_create_config_table', 1),
('2017_06_27_143601_create_sponsors_table', 1),
('2017_06_27_143909_create_schedule_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedule`
--

CREATE TABLE `schedule` (
  `id` int(10) UNSIGNED NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `startHour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endHour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `speaker` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `schedule`
--

INSERT INTO `schedule` (`id`, `day`, `startHour`, `endHour`, `title`, `speaker`, `position`, `created_at`, `updated_at`) VALUES
(8, '10-08-2017', '08:30', '13:30', 'TALLER: Sistemas de Visión Artificial', ' ', ' ', '2017-07-07 23:17:21', '2017-07-07 23:17:21'),
(9, '10-08-2017', '08:30', '13:30', 'TALLER: Adaptado Android a la Manufactura', ' ', ' ', '2017-07-07 23:18:07', '2017-07-07 23:18:07'),
(10, '10-08-2017', '08:30', '13:30', 'TALLER: Robótica en la Manufaltura', ' ', ' ', '2017-07-07 23:18:39', '2017-07-07 23:18:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(5, 'Dataware', '5.png', '2017-07-07 23:15:21', '2017-07-07 23:15:21'),
(6, 'COGNEX', '6.png', '2017-07-07 23:15:42', '2017-07-07 23:15:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ana María González', 'ana.gonzalez@smilemarketing.mx', '$2y$10$OpkVAcKpEBIcIS//pYpkIeRObGs0epwFQ42EaflNeIGWekaXkabP.', 'LaXWndYeM1', '2017-06-30 01:48:10', NULL),
(2, 'Ana María González', 'n.sandoval@dataware.com.mx', '$2y$10$Z8L9sEDQqtmYExsRbg3upOCmAVyv0RNB/WN2C1RSKKitGlSmTIzle', 'VudMD3y3r1', '2017-06-30 01:48:10', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;