<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Schedule extends Model
{
    /**
     * The attributes that are mass assignable.
     *trtr
     * @var array
     */
    protected $fillable = [
        'day', 'startHour', 'endHour', 'title', 'speaker', 'position',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    protected $table = 'schedule';

    /**
     * Create a new Schedule instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public static function store(Request $request){
        $schedule = new Schedule();
        $schedule->day = $request->day;
        $schedule->startHour = $request->startHour;
        $schedule->endHour = $request->endHour;
        $schedule->title = $request->title;
        $schedule->speaker = $request->speaker;
        $schedule->position = $request->position;
        if($schedule->save()){
            return true;
        }
        return false;
    }

    public static function updateSchedule(Request $request, $id){
        $schedule = Schedule::where('id', $id);
        $schedule->day = $request->day;
        $schedule->startHour = $request->startHour;
        $schedule->endHour = $request->endHour;
        $schedule->title = $request->title;
        $schedule->speaker = $request->speaker;
        $schedule->position = $request->position;
        if($schedule->save()){
            return true;
        }
        return false;
    }
}
