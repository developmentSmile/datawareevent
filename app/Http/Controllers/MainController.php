<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Config;
use App\Sponsors;
use App\Schedule;

class MainController extends Controller
{
    public function index(){
    	$config = Config::where('id', 1)->get();
    	$config = $config[0];
    	$sponsors = Sponsors::get(); 
    $schedules = Schedule::select('id', 'day', 'startHour', 'endHour', 'title', 'speaker', 'position')->orderBy('startHour')->get()->groupBy('day');
    echo($schedules);
    	$error = 0;
    	return view('welcome',compact('config','sponsors','schedules','error'));
    }

    public function lead(Request $request){
    	$config = Config::where('id', 1)->get();
    	$config = $config[0];
    	$para = $config["leads"];

    	$nombre = $request->name;
		$email = $request->email;
		$telefono = $request->phone;
		$empresa = $request->brand;
		$position = $request->position;
		$number = $request->number;
		$estados = $request->estados;

		$header = "Dataware" . "\r\nContent-type: text/html\r\n";
		$header = "MIME-Version: 1.0\r\n";
		$header .= "Content-type: text/html; charset=iso-8859-1\r\n";

		//cuerpo del mensaje//
		$mensaje = "---------------------\n";
		$mensaje .= "            Contacto               \n";
		$mensaje .= "---------------------------------- <br> \n";
		$mensaje .= "NOMBRE:  " . $nombre . "<br> \n";
		$mensaje .= "CORREO:  " . $email . "<br> \n";
		$mensaje .= "TELEFONO:  " . $telefono . "<br> \n";
		$mensaje .= "EMPRESA: " . $empresa. " <br>\r\n";
		$mensaje .= "PUESTO: " . $position. " <br>\r\n";
		$mensaje .= "NUMERO EMPLEADOS: " . $number. " <br>\r\n";
		$mensaje .= "ESTADO: " . $estados. " <br>\r\n";
		$mensaje .= "FECHA:    " . date("d/m/Y") . "<br> \n";
		$mensaje .= "HORA:     " . date("h:i:s a") . " <br>\n";

		$mensaje .= "---------------------------------- \n";
		$mensaje .= "Enviado desde -----------------------------<br>http://dataware.com.mx/evento";
		$asunto ="Contacto desde la pagina web de http://dataware.com.mx/evento ...";

		mail($para,$asunto, utf8_decode($mensaje), $header);
		return view('mail',compact('config'));
    }

}
