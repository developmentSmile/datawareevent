<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Sponsors;

class SponsorsController extends Controller
{
    /**
     * Create
     */
    public function createSponsor(Request $request){
        $error = 0;
        $sponsor = new Sponsors();
        $sponsor->name = $request->name;
        $sponsor->image =  "default.png";
        $sponsor->save();

        if($request->hasFile('image')) {
            $name = $sponsor->id . "." . $request->file('image')->getClientOriginalExtension();
            $path = "sponsors";
            $request->file('image')->move(public_path() . "/" . $path, $name);
            $sponsor->image = $name;
            $sponsor->save();
        }

        $sponsors = Sponsors::get();
        return view('sponsors',compact('sponsors','error'));        
    }

    /**
     * Read
     */
    public function readSponsor(){
        $sponsors = Sponsors::get(); 
        $error = 0;
        return view('sponsors',compact('sponsors','error'));
    }

    /**
     * Update
     */
    public function updateSponsor(Request $request, $id){
        $response = Sponsors::updateSponsor($request, $id);

        if($response){
            return response()->json(['success' => true]);
        }

        return response()->json(['error' => 'No se puedo guardar la entrada']);
    }

    /**
     * Delete
     */
    public function deleteSponsor(Request $request){
        $response  = Sponsors::where('id', '=', $request->id)->delete();
        $error = 0;
        if($response){
            $sponsors = Sponsors::get();
            return view('sponsors',compact('sponsors','error'));
        }
        $sponsors = Sponsors::get();
        $error = 1;
        return view('sponsors',compact('sponsors','error'));
    }
}
