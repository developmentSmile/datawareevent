<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Config;
use App\Sponsors;

class ConfigController extends Controller
{
    /**
     * Update Configuration
     */
    public function updateConfig(Request $request){
        $response = Config::updateConfig($request);
        $config = Config::where('id', 1)->get();
        $config = $config[0];
        $error = 0;
        if($response){
            if($request->hasFile('first')) {
                if($request->file('first')->getClientOriginalExtension() == "jpg"){
                    $name ="first.jpg";
                    $path = "images";
                    $request->file('first')->move(public_path() . "/" . $path, $name);
                }else{
                    $error = 2;
                }
            }

            if($request->hasFile('second')) {
                if($request->file('second')->getClientOriginalExtension() == "jpg"){
                    $name ="second.jpg";
                    $path = "images";
                    $request->file('second')->move(public_path() . "/" . $path, $name);
                }else{
                    $error = 2;
                }
            }

            return view('home',compact('config','error'));
        }
        $error = 1;
        return view('home',compact('config','error'));
    }

    /**
     * Read all Configuration
     */
    public function readConfig(){
        $config = Config::where('id', 1)->get();
        $config = $config[0]; 
        $error = 0;
        return view('home',compact('config','error'));
    }

}
