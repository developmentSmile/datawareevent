<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Schedule;

class ScheduleController extends Controller
{
    /**
     * Create
     */
    public function createSchedule(Request $request){
        $response = Schedule::store($request);
        $schedules = Schedule::get();
        $error = 0;
        if($response){
            return view('schedule',compact('schedules','error'));
        }
        $error = 1;
        return view('schedule',compact('schedules','error'));
    }

    /**
     * Read
     */
    public function readSchedule(){
        //$schedules = Schedule::select('id', 'day', 'startHour', 'endHour', 'title', 'speaker', 'position')->orderBy('startHour')->get()->groupBy('day');
        $schedules = Schedule::get();
        $error = 0;
        return view('schedule',compact('schedules','error'));
    }

    /**
     * Update
     */
    public function updateSchedule(Request $request, $id){
        $response = Schedule::updateSchedule($request, $id);
        $schedules = Schedule::get();
        $error = 0;

        if($response){
            return view('schedule',compact('schedules','error'));
        }
        $error = 1;
        return view('schedule',compact('schedules','error'));
    }

    /**
     * Delete
     */
    public function deleteSchedule(Request $request){
        $response  = Schedule::where('id', '=', $request->id)->delete();
        $error = 0;
        if($response){
            $schedules = Schedule::get();
            return view('schedule',compact('schedules','error'));
        }
        $schedules = Schedule::get();
        $error = 1;
        return view('schedule',compact('schedules','error'));
    }
}
