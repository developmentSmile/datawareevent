<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::post('/lead', 'MainController@lead');

Route::get('beadmin', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::post('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

//Config
Route::get('/config', 'ConfigController@readConfig');
Route::post('/config', 'ConfigController@updateConfig');

//Sponsors
Route::post('/sponsor', 'SponsorsController@createSponsor');
Route::get('/sponsor', 'SponsorsController@readSponsor');
Route::put('/sponsor', 'SponsorsController@updateSponsor');
Route::get('/deletesponsor', 'SponsorsController@deleteSponsor');

//Schedule
Route::post('/schedule', 'ScheduleController@createSchedule');
Route::get('/schedule', 'ScheduleController@readSchedule');
Route::put('/schedule', 'ScheduleController@updateSchedule');
Route::get('/deleteschedule', 'ScheduleController@deleteSchedule');