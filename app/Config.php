<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Config extends Model
{
    /**
     * The attributes that are mass assignable.
     *trtr
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'keywords', 'when', 'whenString', 'whereMain', 'whereSecond',
        'lat', 'long', 'leads','duration'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token','actionButton','facebook', 'twitter',
        'googleplus', 'linkedin', 'youtube','phone', 'email', 'url'
    ];

    protected $table = 'config';

    /**
     * Create a new Config instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public static function updateConfig(Request $request){
        $config = Config::where('id', 1)->first();
        $config->title = $request->title;
        $config->description = $request->description;
        $config->keywords = $request->keywords;
        $config->when = $request->when;
        $config->whenString = $request->whenString;
        $config->whereMain = $request->whereMain;
        $config->whereSecond = $request->whereSecond;
        $config->lat = $request->lat;
        $config->long = $request->long;
        $config->leads = $request->leads;
        if($config->save()){
            return true;
        }
        return false;
    }
}
