<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Sponsors extends Model
{
    /**
     * The attributes that are mass assignable.
     *trtr
     * @var array
     */
    protected $fillable = [
        'name', 'image'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    protected $table = 'sponsors';

    /**
     * Create a new Sponsors instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public static function store(Request $request){
        $sponsor = new Sponsors();
        $sponsor->name = $request->name;
        $sponsor->image = $request->image;

        if($sponsor->save()){
            return true;
        }
        return false;
    }

    public static function updateSponsor(Request $request, $id){
        $sponsor = Sponsors::where('id', $id);
        $sponsor->name = $request->name;
        $sponsor->image = $request->image;

        if($sponsor->save()){
            return true;
        }
        return false;
    }
}
