@extends('layouts.admin')

@section('content')
<div id="login-page">
        <div class="container">
        
              <form class="form-login" role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
                <h2 class="form-login-heading">Inicio de sesión</h2>
                <div class="login-wrap">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo">
                    <br>
                    <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña">
                    <label class="checkbox">
                        <span class="pull-right">
                            <a></a>
        
                        </span>
                    </label>
                    <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> Inicio Sesión</button>
        
                </div>
        
              </form>       
        
        </div>
      </div>
@endsection
@section('js')
   <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="admin/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("admin/img/login-bg.jpg", {speed: 500});
    </script>
@endsection
