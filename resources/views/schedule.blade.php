@extends('layouts.admin')

@section('content')

@include('layouts.topbar')

@include('layouts.sidebar')
<section id="main-content">
          <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Agenda</h3>
            
            @if ($error == 1)
                <div class="alert alert-danger" role="alert">Hubo un error.Intente nuevamente</div>
            @endif

            <div class="row mt">
                  <div class="col-md-12">
                      <div class="form-panel">
                      <form class="form-horizontal style-form" method="POST" action="/schedule">
                        {{csrf_field()}}
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Titulo</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="title" required="">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Ponente</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="speaker" required="">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Puesto laboral</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="position" required="">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Día</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control datepicker" name="day" required="">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Hora de Inicio</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="startHour" required="" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" title="Colocar hora HH:MM" maxlength="5" minlength="5" placeholder="00:00">
                                  <span class="help-block">Formato 24 horas</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Hora de Fin</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="endHour" required="" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" title="Colocar hora HH:MM" maxlength="5" minlength="5" placeholder="00:00">
                                  <span class="help-block">Formato 24 horas</span>
                              </div>
                          </div>

                          <button type="submit" class="btn btn-success center-block">Guardar</button>

                      </form>
                    </div>
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

            <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                            <h4><i class="fa fa-angle-right"></i>Lista</h4>
                            <hr>
                              <thead>
                              <tr>
                                  <th>Titulo</th>
                                  <th>Ponente</th>
                                  <th>Puesto</th>
                                  <th>Día</th>
                                  <th>Inicio</th>
                                  <th>Fin</th>
                                  <th></th>
                              </tr>
                              </thead>

                              <tbody>
                              @foreach ($schedules as $event)
                                <tr>
                                  <td>{{ $event->title }}</td>
                                  <td>{{ $event->speaker }}</td>
                                  <td>{{ $event->position }}</td>
                                  <td>{{ $event->day }}</td>
                                  <td>{{ $event->startHour }}</td>
                                  <td>{{ $event->endHour }}</td>
                                  <td>
                                  <form method="GET" action="/deleteschedule">
                                      <input type="text" name="id" value="{{ $event->id }}" class="hidden">
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </form> 
                                  </td>
                              </tr>
                              @endforeach
                              
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
           
            
            
        </section>
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>
@endsection
@section('js')
  <script>
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
</script>

@endsection