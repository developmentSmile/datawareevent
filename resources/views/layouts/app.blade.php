<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ $config->title }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/animate.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <meta name="description" content="Asiste a nuestra conferencia / desayuno gratuito en León. Hablaremos de cómo hacer tu empresa más rentable y eficientar tus procesos. Regístrate aquí!">
    <meta name="keywords" content="Industria del Calzado,Conferencia,Eventos león ,Pymes,Soluciones tecnológicas,SAP,ZEBRA">

</head>
<body id="app-layout">
    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- back to top ends! -->
        <script type="text/javascript" src="js/owl.carousel.min.js"></script> 
        <script type="text/javascript" src="js/animation.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0l-vybpDJI1NtjPGQmV_dRS5xyUtKIQM&callback=initMap"
    async defer></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
