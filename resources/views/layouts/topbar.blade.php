<header class="header black-bg">
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--logo start-->
    <a href="/home" class="logo"><b>PANEL ADMIN</b></a>
    <!--logo end-->

    <div class="top-menu">
        <ul class="nav pull-right top-menu">
            <li><a  class="logout" href="{{ route('auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
        </ul>
    </div>

    <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

        </header>
      <!--header end-->