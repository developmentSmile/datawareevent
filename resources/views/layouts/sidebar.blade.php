      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <h5 class="centered"></h5>
                    
                  <li class="mt">
                      <a class="" href="/config">
                          <i class="fa fa-dashboard"></i>
                          <span>Configuracion</span>
                      </a>
                  </li>

                  <li class="mt">
                      <a class="" href="/sponsor">
                          <i class="fa fa-money"></i>
                          <span>Patrocinadores</span>
                      </a>
                  </li>

                  <li class="mt">
                      <a class="" href="/schedule">
                          <i class="fa fa-calendar-o"></i>
                          <span>Agenda</span>
                      </a>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->