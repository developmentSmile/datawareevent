@extends('layouts.admin')

@section('content')

@include('layouts.topbar')

@include('layouts.sidebar')
      <section id="main-content">
          <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Configuración General</h3>
            
            @if ($error == 1)
                <div class="alert alert-danger" role="alert">No se puedo guardar la configuración.Intente nuevamente</div>
            @endif
            @if ($error == 2)
                <div class="alert alert-danger" role="alert">No se pudieron actualizar las imágenes de fondo.</div>
            @endif


            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">

                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="POST" action="/config" enctype="multipart/form-data">
                        {{csrf_field()}}
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Titulo</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="title" value="{{ $config->title }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Descripcion Seo</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="description" value="{{ $config->description }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Keywords Seo</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="keywords" value="{{ $config->keywords }}">
                                  <span class="help-block">Separadas por coma</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Cuando</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="when" value="{{ $config->when }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Cuando (segunda línea)</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="whenString" value="{{ $config->whenString }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Días que dura el evento</label>
                              <div class="col-sm-10">
                                  <input type="number" class="form-control" name="duration" value="{{ $config->duration }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Donde</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="whereMain" value="{{ $config->whereMain }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Estado</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="whereSecond" value="{{ $config->whereSecond }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Correo de Leads</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="leads" value="{{ $config->leads }}">
                                  <span class="help-block">Separados por comas</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Latitud</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="lat" value="{{ $config->lat }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Longitud</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="long" value="{{ $config->long }}">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Fondo principal</label>
                              <div class="col-sm-10">
                                  <input type="file" class="" name="first">
                                  <span class="help-block">La imágen debe de ser JPG</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Fondo registro</label>
                              <div class="col-sm-10">
                                  <input type="file" class="" name="second">
                                  <span class="help-block">La imágen debe de ser JPG</span>
                              </div>
                          </div>

                          <button type="submit" class="btn btn-success center-block">Guardar</button>

                      </form>
                  </div>

                </div><!-- col-lg-12-->         
            </div><!-- /row -->
           
            
            
        </section>
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

@endsection
