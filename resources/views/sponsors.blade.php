@extends('layouts.admin')

@section('content')

@include('layouts.topbar')

@include('layouts.sidebar')
<section id="main-content">
          <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Patrocinadores</h3>
            
            @if ($error == 1)
                <div class="alert alert-danger" role="alert">Hubo un error.Intente nuevamente</div>
            @endif

            <div class="row mt">
                  <div class="col-md-12">
                      <div class="form-panel">
                      <form class="form-horizontal style-form" method="POST" action="/sponsor" enctype="multipart/form-data">
                        {{csrf_field()}}
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="name" required="">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Logo (292 × 94) </label>
                              <div class="col-sm-10">
                                  <input type="file" class="form-control" name="image" required="">
                              </div>
                          </div>

                          <button type="submit" class="btn btn-success center-block">Guardar</button>

                      </form>
                    </div>
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

            <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                            <h4><i class="fa fa-angle-right"></i>Lista</h4>
                            <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Nombre</th>
                                  <th></th>
                              </tr>
                              </thead>

                              <tbody>
                              @foreach ($sponsors as $sponsor)
                                <tr>
                                  <td>{{ $sponsor->name }}</td>
                                  <td>
                                  <form method="GET" action="/deletesponsor">
                                      <input type="text" name="id" value="{{ $sponsor->id }}" class="hidden">
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </form> 
                                  </td>
                              </tr>
                              @endforeach
                              
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
           
            
            
        </section>
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

@endsection