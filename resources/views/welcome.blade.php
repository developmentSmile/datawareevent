@extends('layouts.app')

@section('content')
<!--Main Information-->
        <div class="header">
            <div class="container">
                <!--logo-->
                <nav class="navbar navbar-transparent">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <a class="navbar-brand" href="#index.html">
                        <img alt="Logo" src="images/logoblanco.png" class="pull-left">
                      </a>
                    </div>
                  </div>
                </nav>
                <!--/logo-->
                <h1 class="title">{{ $config->title }}</h1>
                
                <div class="row">
                    <div class="when col-md-6">
                        <div class="icon-holder col-md-4 col-xs-6">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <div class="col-md-8 col-xs-6">
                            <p><strong>Fecha</strong></p>
                            <p><span><b>{{ $config->when }}</b></span><br />{{ $config->whenString }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="where col-md-6">
                        <div class="icon-holder col-md-4 col-xs-6">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="col-md-8 col-xs-6">
                          <p><strong>Ubicación</strong></p>
                          <p><span><b>{{ $config->whereMain }}</b></span><br/>{{ $config->whereSecond }}</p>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="register-now col-md-4">
                        <a href="#registration" class="button">{{ $config->actionButton }}</a>
                    </div>
                </div>

            </div>
        </div>
        <!--Main Information-->

    <!-- topics -->
  <div class="topics">
    <div class="container" style="padding-bottom: 0px;">
      <h2>Agenda de Evento</h2>

      <div class="tabs center-block col-md-8">
        <ul class="text-center list-inline dateul">
          @foreach($schedules as $key=>$item)
            <li onclick="showTab('{{ $key }}')" >{{ $key }}</li>
          @endforeach
        </ul>
      </div>

    </div>
  </div>
  <!-- topics ends! -->

  <!-- schedule -->
  <div class="schedule">
    <div class="container border">
      <div class="tabs">
      
        @foreach($schedules as $key=>$item)
          <!-- day -->
        <div id="tabs-{{ $key }}" class="day tabs-{{ $key }}">
          @foreach ($item as $unit)
          
            @if ($unit->speaker == "")
                <!-- single event -->
                <div class="event">
                  <div class="event-time">{{ $unit->startHour }} - {{ $unit->endHour }}<span></span></div>
                  <div class="event-info">
                    <div>
                      <h4>{{ $unit->title }}</h4>
                    </div>
                  </div>
                </div>
                <!-- single event ends! -->
            @else
          <!-- single event -->
          <div class="event extend">
            <div class="event-time">{{ $unit->startHour }} - {{ $unit->endHour }} <span><i class="fa fa-angle-up"></i></span></div>
            <div class="event-info">
                <div>
                  <h4><a href="#">{{ $unit->title }}</a></h4>
                  <p class="speaker"><strong>{{ $unit->speaker }}</strong><br/>{{ $unit->position }}</p>
                </div>
            </div>
          </div>
          <!-- single event ends! -->
            @endif

          @endforeach
        </div> 
        @endforeach
        
      </div>
    </div>
  </div>
  <!-- schedule ends! -->

  <!-- sponsors -->
  <div class="sponsors">
    <div class="container">
    <h2>Patrocinadores</h2>


    <div id="owl-demo" class="owl-carousel owl-theme">
      @foreach ($sponsors as $sponsor)
        <div class="item"><img src="sponsors/{{ $sponsor->image }}" class="img-responsive" align="{{ $sponsor->name }}"></div>
      @endforeach        
    </div>

    </div>
  </div>
  <!-- sponsors ends! -->

        <!-- form -->
        <div class="location registration" id="registration">
            <div class="container">
              <h2>Registro Gratuito</h2>

              <!-- info -->
              <div class="col-md-6 info">
                <div class="form">
                  <form method="POST" action="/lead">
                    {{csrf_field()}}
                    <input type="text" name="name" placeholder="Nombre completo" required=""/>
                    <input type="email" name="email" placeholder="Email" required=""/>
                    <input id="phone" name="phone" placeholder="Teléfono" class="form-control input-md" required="" type="text" pattern="\d*" title="Colocar unicamente números" maxlength="10" minlength="10">
                    <input type="text" name="brand" placeholder="Compañía" required=""/>
                    <input type="text" name="position" placeholder="Puesto" required=""/>
                    <select name="number" required="">
                      <option value="0">Número de empleados</option>
                      <option value="0a10">0 a 10 empleados</option>
                      <option value="11a50">11 a 50 empleados</option>
                      <option value="51a100">51 a 100 empleados</option>
                      <option value="mas de 100">más de 100 empleados</option>
                    </select>
                    <select name="estados" required="">
                      <option value="0">Selecciona una estado</option>
                      <option value="Aguascalientes">Aguascalientes</option>
                      <option value="Baja California">Baja California</option>
                      <option value="Baja California Sur">Baja California Sur</option>
                      <option value="Campeche">Campeche</option>
                      <option value="Ciudad de México">Ciudad de México</option>
                      <option value="Coahuila">Coahuila de Zaragoza</option>
                      <option value="Colima">Colima</option>
                      <option value="Chiapas">Chiapas</option>
                      <option value="Chihuahua">Chihuahua</option>
                      <option value="Durango">Durango</option>
                      <option value="Guanajuato">Guanajuato</option>
                      <option value="Guerrero">Guerrero</option>
                      <option value="Hidalgo">Hidalgo</option>
                      <option value="Jalisco">Jalisco</option>
                      <option value="México">México</option>
                      <option value="Michoacán">Michoacán de Ocampo</option>
                      <option value="Morelos">Morelos</option>
                      <option value="Nayarit">Nayarit</option>
                      <option value="Nuevo León">Nuevo León</option>
                      <option value="Oaxaca">Oaxaca</option>
                      <option value="Puebla">Puebla</option>
                      <option value="Querétaro">Querétaro</option>
                      <option value="Quintana Roo">Quintana Roo</option>
                      <option value="San Luis Potosí">San Luis Potosí</option>
                      <option value="Sinaloa">Sinaloa</option>
                      <option value="Sonora">Sonora</option>
                      <option value="Tabasco">Tabasco</option>
                      <option value="Tamaulipas">Tamaulipas</option>
                      <option value="Tlaxcala">Tlaxcala</option>
                      <option value="Veracruz">Veracruz de Ignacio de la Llave</option>
                      <option value="Yucatán">Yucatán</option>
                      <option value="Zacatecas">Zacatecas</option>
                      </select>
                    <button name="submit" type="submit">Enviar</button>
                  </form>
                </div>
              </div>
              <!-- info ends! -->

             <div class="col-md-6 info hidden-sm hidden-xs center-block text-center">
                <div id="map" style="height: 400px"></div>
             </div>
            </div>
        </div>
  <!-- form ends! -->

  <div class="space"></div>

    <!-- social -->
      <div class="social">
        <div class="container">
            <h2 class="white">Mantente informado</h2>
            <p class="subtitle white">Regístrate para estar enterado de nuestros próximos eventos</p>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <form action="" method="post" action="send.php">
                      <input type="text" name="email" placeholder="email" />
                      <button name="submit" type="submit"><i class="fa fa-arrow-circle-right"></i></button>
                    </form>
                </div>
                <div class="col-md-4 col-xs-12">
                    <a href="{{ $config->facebook }}"><i class="fa fa-facebook"></i></a>
                    <a href="{{ $config->twitter }}"><i class="fa fa-twitter"></i></a>
                    <a href="{{ $config->googleplus }}"><i class="fa fa-google-plus"></i></a>
                    <a href="{{ $config->linkedin }}"><i class="fa fa-linkedin"></i></a>
                    <a href="{{ $config->youtube }}"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
        </div>
      </div>
      <!-- social ends! -->


        <!-- footer -->
        <div class="footer bgwhite">
            <div class="container">
                <p>Datos de contacto:</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i> {{ $config->phone }}  &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-envelope" aria-hidden="true"></i> {{ $config->email }}  &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ $config->url }}">{{ $config->url }}</a></p>
            </div>
        </div>
      <!-- footer ends! -->
        <!-- back to top -->
        <div class="back">
          <div class="container">

            <a href="#top" id="top" class="top"><i class="fa fa-angle-up fa-3x"></i></a>

          </div>
        </div>

        <script type="text/javascript">
          window.onload = function() {
            $(".day").hide();
            $(".day:first").show();

          };

          function showTab(idp) {
            var idp = "tabs-"+idp;
            $(".day").hide();
            $("."+idp).show();
          }

          function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: {{ $config->lat }}, lng: {{ $config->long }}},
              zoom: 15
          });

            var iconBase = 'http://marketing-queretaro.com/markers/';

            var marker = new google.maps.Marker({
              position: new google.maps.LatLng({{ $config->lat }}, {{ $config->long }}),
              map: map,
              icon: iconBase + '32x32amarillo.png',
              title: '¡Encuentranos aquí!'
            });

          }
        </script>
@endsection
